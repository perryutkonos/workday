'use strict';

let webpack = require('webpack');
module.exports = {

    entry: {
        app: "./src/index",
    },

    output: {
        path: __dirname + '/build',
        filename: '[name].js'
    },

    watch: true,

    module: { //Обновлено
        loaders: [ //добавили babel-loader
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    "presets": ["env", "stage-0", "react"],
                }
            }, {
                test: /\.css/,
                loader: 'style-loader!css-loader?resolve url'

            }, {
                test: /\.(html)$/,
                loader: 'file-loader?name=[name].[ext]'
            }
        ]
    }
}