export default {

    userData: {
        name: "Роман",
        rate: 1000
    },
    tasks:[
        {
            id:0,
            title: "Задача 1",
            description: "Тупая правка",
            time: 2,
        }, {
            id:1,
            title: "Задача 2",
            description: "Еще одна правка",
            time: 3,
        }
    ]
}