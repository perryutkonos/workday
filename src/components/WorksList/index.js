import React, {Component} from 'react'
import Task from "../Task"

export default class WorksList extends Component {
    
    render() {
        
        const tasks = this.props.tasks
        const rate = this.props.rate;
        
        const taskElements = tasks.map((task, index) =>
           <Task key={task.id} task={task} rate={rate}/>
        )
        
        return (
            <div className="card-list">
                {taskElements}
            </div>
        )
    }
}