import React, {Component} from 'react'

import Totals from "../Totals"
import WorksList from "../WorksList"
import Form from "../Form"

import data from "../../data"
import 'bootstrap/dist/css/bootstrap.css'

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: {}
        }
    }

    componentWillMount () {
        this.setState({
            data: data
        })
    }

    render() {
        const data = {...this.state.data};
        const lastId = data.tasks[data.tasks.length - 1].id
      
        return (
            <div className="container">
                <header className="jumbotron bg-primary text-white">
                    <h1 className="display-4">И че ты сегодня сделал??</h1>
                </header>
                <Totals tasks={data.tasks.slice()} rate={data.userData.rate}/>
                <WorksList tasks={data.tasks.slice()} rate={data.userData.rate}/>
                <Form addTask={this.handleClick.bind(this)} lastId={lastId}/>
            </div>
        )
    }
    
    handleClick(newTask) {
        let newData = this.state.data;
        newData.tasks.push(newTask);
        this.setState({
            data: newData
        })
    }
}