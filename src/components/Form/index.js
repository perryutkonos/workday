import React, {Component} from 'react'
import "./style.css"

export default class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id:'',
            title: '',
            description: "",
            time: ""
        }
    }
    
    componentWillMount() {
        console.log("Form",this.props.lastId)
        this.setState({
            id: this.props.lastId+1
        })
    }

    componentWillReceiveProps(nextProps){
        console.log("Form",nextProps.lastId)
        this.setState({
            id:nextProps.lastId+1
        })
    }
    
    handleInputChange(event) {

        const target = event.target;
        const name = target.name;

        const value = (target.name==="time") ? Number(target.value) : target.value;
        this.setState({
            [name]: value
        });
        
        console.log(this.state)
    }
    
    addTask(){
        //console.log(this.state);
        this.props.addTask(this.state);
        this.setState({
            id:'',
            title: '',
            description: "",
            time: ""
        });
    }

    render() {
        return (
            <div className="jumbotron mt-5">
                <h2 className="display-4">А что еще сделал?</h2>
                <form action="">
                    <div className="form-group mt-4">
                        <div className="input-group">
                            <input type="text"
                                   className="form-control"
                                   placeholder="Название задачи"
                                   name="title"
                                   value={this.state.title}
                                   onChange={this.handleInputChange.bind(this)}
                            />
                        </div>
                        <div className="input-group mt-3">
                            <textarea
                                className="form-control custom"
                                placeholder="Описание задачи"
                                name="description"
                                onChange={this.handleInputChange.bind(this)}
                                value={this.state.description}
                            />
                        </div>
                        <div className="input-group mt-3">
                            <input type="text"
                                   className="form-control"
                                   placeholder="Время"
                                   name="time"
                                   onChange={this.handleInputChange.bind(this)}
                                   value={this.state.time}

                            />
                        </div>

                        <div className="btn-group mt-3">
                            <button type="button" onClick={this.addTask.bind(this)} className="btn btn-primary">Молодец</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

    
}