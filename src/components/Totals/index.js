import React, {Component} from 'react'

export default class Totals extends Component {
    
    constructor(props) {
        super(props)
        this.totalTime = 0;
        this.profit = 0;
    }
    
    componentWillMount() {
        this.props.tasks.forEach((task) => {
            this.totalTime+=task.time
        })
        this.profit = this.totalTime * this.props.rate;
    }
    componentWillReceiveProps(nextProps){
        this.totalTime = 0;
        nextProps.tasks.forEach((task) => {
            this.totalTime+=task.time
        })
        this.profit = this.totalTime * nextProps.rate;
    }
    render() {
        return (
            <table className="table bg-danger text-white">
                <thead>
                <tr>
                    <th scope="col">Потрачено времени</th>
                    <th scope="col">Заработано денег</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{this.totalTime}</td>
                    <td>{ this.profit}</td>
                </tr>
                </tbody>
            </table>
        )
    }
}