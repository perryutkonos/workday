import React, {Component} from 'react'

export default class Task extends Component {
    render() {
        const {title,description,time} = this.props.task;
        const rate = this.props.rate;
        return (
            <div className="card bg-light mt-3">
                <div className="card-body bg-dark text-white">
                    <h5 className="card-title">{title}</h5>
                    <p className="card-text">{description}</p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">Время - {time}</li>
                    <li className="list-group-item">Доход - {time*rate}руб</li>
                </ul>
            </div>
        )
    }
}